package model.data_structures;

import org.junit.*;

public class StackTest <E>
{
	private Stack<E> stack;

	@Before
	public void isInstantiatedWithNew() 
	{
		new Stack<E>();
	}


	public class WhenNew 
	{
		@Before
		public void createNewStack() 
		{
			stack = new Stack<>();
		}

		@Test
		public void isEmpty() 
		{
			Assert.assertTrue(stack.isEmpty());
		}

		@Test
		public void throwsExceptionWhenPopped() 
		{
			try
			{
				stack.pop();
				Assert.fail();
			}
			catch (Exception e)
			{
				// Deber�a de entrar aqu�
			}
		}

		@Test
		void throwsExceptionWhenPeeked() 
		{
			try
			{
				stack.peek();
				Assert.fail();
			}
			catch (Exception e)
			{
				// Deber�a de entrar aqu�
			}
		}

		public class AfterPushing 
		{
			private Stack<String> stack;
			
			public AfterPushing()
			{
				stack = new Stack<String>();
			}
			
			String anElement = "an element";

			@Test
			public void pushAnElement() 
			{
				stack.push(anElement);
			}

			@Test
			public void isNotEmpty() 
			{
				Assert.assertFalse(stack.isEmpty());
			}

			@Test
			public void returnElementWhenPopped() 
			{
				Assert.assertEquals(anElement, stack.pop());
				Assert.assertTrue(stack.isEmpty());
			}

			@Test
			public void returnElementWhenPeeked() 
			{
				Assert.assertEquals(anElement, stack.peek());
				Assert.assertFalse(stack.isEmpty());
			}
		}
	}
}
