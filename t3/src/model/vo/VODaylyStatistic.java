package model.vo;

public class VODaylyStatistic 
{
	//TODO
	private int year;
	
	private int month;
	
	private int day;
	
	private int accidents;
	
	private int total;

	public VODaylyStatistic(int year, int month, int day, int accidents, int total) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.accidents = accidents;
		this.total = total;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getAccidents() {
		return accidents;
	}

	public void setAccidents(int accidents) {
		this.accidents = accidents;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
}
