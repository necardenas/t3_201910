package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Scanner;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller 
{
 
	private MovingViolationsManagerView view;
	
	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;
	
	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;


	public Controller() {
		view = new MovingViolationsManagerView();
		
		//TODO, inicializar la pila y la cola
		movingViolationsQueue = new Queue<VOMovingViolations>();
		movingViolationsStack = new Stack<VOMovingViolations>();
	}
	
	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		
		while(!fin)
		{
			view.printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					this.loadMovingViolations();
					break;
					
				case 2:
					IQueue<VODaylyStatistic> dailyStatistics = this.getDailyStatistics();
					view.printDailyStatistics(dailyStatistics);
					break;
					
				case 3:
					
					try
					{
						view.printMensage("Ingrese el número de infracciones a buscar");
						int n = sc.nextInt();

						IStack<VOMovingViolations> violations = this.nLastAccidents(n);
						view.printMovingViolations(violations);
						break;
					}
					
					catch (Exception e) 
					{
						e.printStackTrace();
					}
											
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	

	public void loadMovingViolations() 
	{
		try
		{
			String pathJan = "*\\data\\Moving_Violations_Issued_in_January_2018.csv";
			CSVReader readerJan = new CSVReaderBuilder(new BufferedReader( new FileReader(new File (pathJan)) )).withSkipLines(1).build();
			
			String pathFeb = "*\\data\\Moving_Violations_Issued_in_February_2018";
			CSVReader readerFeb = new CSVReaderBuilder(new BufferedReader( new FileReader(new File(pathFeb)) )).withSkipLines(1).build();
			
			// Path Jan
			
			String[] rowJan = null;
			
			while ((rowJan = readerJan.readNext()) != null)
			{
				String in1 = rowJan[0];
				Integer objId = Integer.parseInt(in1);
				
				String location = rowJan[1];
				
				String in2 = rowJan[2];
				Integer adressId = Integer.parseInt(in2);
				
				String in3 = rowJan[3];
				Integer streetId = Integer.parseInt(in3);
				
				String in4 = rowJan[4];
				Double xCord = Double.parseDouble(in4);
				
				String in5 = rowJan[5];
				Double yCord = Double.parseDouble(in5);
				
				String ticketType = rowJan[6];
				
				String in6 = rowJan[7];
				Integer fineAmt = Integer.parseInt(in6);
				
				String in7 = rowJan[8];
				Integer totalPaid = Integer.parseInt(in7);
				
				String in8 = rowJan[9];
				Integer penalty1 = 	Integer.parseInt(in8);
				
				String accident = rowJan[10];
				
				String date = rowJan[11].substring(0, 11);
				
				String vioCode = rowJan[12];
				
				String vioDesc = rowJan[13];
				
				VOMovingViolations vomv = new VOMovingViolations(objId, location, adressId, streetId, xCord, yCord, ticketType, fineAmt, totalPaid, penalty1, accident, date, vioCode, vioDesc);
				movingViolationsQueue.enqueue(vomv);
				movingViolationsStack.push(vomv);
			}
			
			//Path Feb
			
			String[] rowFeb = null;

			while ((rowFeb = readerFeb.readNext()) != null)
			{
				String in1 = rowFeb[0];
				Integer objId = Integer.parseInt(in1);

				String location = rowFeb[1];

				String in2 = rowFeb[2];
				Integer adressId = Integer.parseInt(in2);

				String in3 = rowFeb[3];
				Integer streetId = Integer.parseInt(in3);

				String in4 = rowFeb[4];
				Double xCord = Double.parseDouble(in4);

				String in5 = rowFeb[5];
				Double yCord = Double.parseDouble(in5);

				String ticketType = rowFeb[6];

				String in6 = rowFeb[7];
				Integer fineAmt = Integer.parseInt(in6);

				String in7 = rowFeb[8];
				Integer totalPaid = Integer.parseInt(in7);

				String in8 = rowFeb[9];
				Integer penalty1 = 	Integer.parseInt(in8);

				String accident = rowFeb[10];

				String date = rowFeb[11].substring(0, 11);

				String vioCode = rowFeb[12];

				String vioDesc = rowFeb[13];

				VOMovingViolations vomv = new VOMovingViolations(objId, location, adressId, streetId, xCord, yCord, ticketType, fineAmt, totalPaid, penalty1, accident, date, vioCode, vioDesc);
				movingViolationsQueue.enqueue(vomv);
				movingViolationsStack.push(vomv);
			}

			readerJan.close();
			readerFeb.close();
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public IQueue <VODaylyStatistic> getDailyStatistics () throws NumberFormatException
	{
		IQueue<VODaylyStatistic> retrn = new Queue<VODaylyStatistic>();
		IQueue<VOMovingViolations> temp = movingViolationsQueue;
		String currentDate = "";
		VODaylyStatistic dailyStats = null;

		while (temp.size() != 0)
		{
			VOMovingViolations current = temp.dequeue();
			String day = current.getTicketIssueDate().substring(9);

			if (day.startsWith("0"))
			{
				day = day.substring(1);
			}

			Integer dei = Integer.parseInt(day);

			String monthh = current.getTicketIssueDate().substring(7,8);
			Integer month = Integer.parseInt(monthh);
			
			if ( currentDate != (day + month) )
			{
				if (currentDate != "")
				{
					retrn.enqueue(dailyStats);
				}
				
				currentDate = day + month;

				dailyStats = new VODaylyStatistic(2018, month, dei, 0, 0);

				int accident = (current.getAccidentIndicator().equalsIgnoreCase("Yes"))?1:0;
				dailyStats.setAccidents( dailyStats.getAccidents() + accident );

				int fineAmt = current.getfineAmt();
				dailyStats.setTotal( dailyStats.getTotal() + fineAmt );
			}

			else
			{
				int accident = (current.getAccidentIndicator().equalsIgnoreCase("Yes"))?1:0;
				dailyStats.setAccidents( dailyStats.getAccidents() + accident );

				int fineAmt = current.getfineAmt();
				dailyStats.setTotal( dailyStats.getTotal() + fineAmt );
			}
		}
		
		return retrn;
	}

	
	public IStack <VOMovingViolations> nLastAccidents(int n) throws Exception
	{
		if (n > 0)
		{
			IStack<VOMovingViolations> temp = movingViolationsStack;
			IStack<VOMovingViolations> retrn = new Stack<VOMovingViolations>();

			Integer accidents = 0;

			while (temp.size() != 0 && accidents != n)
			{
				VOMovingViolations current = temp.pop();

				if (current.getAccidentIndicator().equalsIgnoreCase("Yes"))
				{
					retrn.push(current);
					
					System.out.println(current.objectId() + " " + current.getTicketIssueDate() + " " + current.getLocation() + " " + current.getViolationDescription());
					
					accidents++;
				}
			}
			
			return retrn;
		}
		
		else
		{
			throw new Exception ("n Debe ser mayor que 0");
		}
	}
}
